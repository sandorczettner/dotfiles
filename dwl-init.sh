#!/bin/sh

#xprop -root -f _XWAYLAND_GLOBAL_OUTPUT_SCALE 32c -set _XWAYLAND_GLOBAL_OUTPUT_SCALE 2

fnott 2>&1 >/dev/null &
pipewire &
pipewire-pulse &
wireplumber &

yambar -c /home/sandor/.config/yambar/dwl.yml &
yambar -c /home/sandor/.config/yambar/dwl2.yml &
#trayer --edge top --width 10 --align right --monitor 0 --height 22 --margin 2 &
fnott 2>&1 >/dev/null &
wbg /usr/share/wallpapers/forest.jpg &

keepassxc &
nextcloud &
thunderbird &
slack --enable-features=WebRTCPipeWireCapturer --enable-features=UseOzonePlatform --ozone-platform=wayland &
swayidle -w &
