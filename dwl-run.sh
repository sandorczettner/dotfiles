#!/bin/sh
export LIBVA_DRIVER_NAME=nvidia
export XDG_SESSION_TYPE=wayland
export GBM_BACKEND=nvidia-drm
export __GLX_VENDOR_LIBRARY_NAME=nvidia
export SDL_VIDEODRIVER=wayland

export WLR_NO_HARDWARE_CURSORS=1
export QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_QPA_PLATFORMTHEME=qt5ct
export QT_QPA_PLATFORM=wayland
export GDK_DPI_SCALE=0.5
export MOZ_ENABLE_WAYLAND=1
export GTK_THEME=oomox-Sandor-Green
export GTK_ICON_THEME=oomox-Sandor-Green
export _JAVA_AWT_WM_NONREPARENTING=1

# slstatus 2>&1 >/dev/null &
# dunst 2>&1 >/dev/null &
# nextcloud 2>&1 >/dev/null &
# nm-applet 2>&1 >/dev/null &
# nitrogen --restore 2>&1 >/dev/null &
# picom --experimental-backends 2>&1 >/dev/null &
# xautolock -time 10 -locker slock 2>&1 >/dev/null &

dbus-launch /home/sandor/Projects/dotfiles/suckless/dwl/dwl > ~/.cache/dwltags
