#!/bin/sh
export QT_QPA_PLATFORMTHEME=qt5ct
export GTK_THEME=oomox-sandor-arc-dark
export GTK_ICON_THEME=oomox-sandor-arc-dark
export QT_QPA_PLATFORM="xcb"
export SDL_VIDEODRIVER="x11"
pipewire &
pipewire-pulse &
wireplumber &
dunst &
nitrogen --restore &
dwmblocks &
paplay ~/Music/Sounds/pc_up.wav &
xset dpms 1200 1300 1400
v4l2-ctl -d /dev/video0 -c zoom_absolute=70
xrdb -merge ~/.Xresources
picom --xrender-sync-fence &
nm-applet --indicator &
exec dwm
