#!/bin/sh
export LIBVA_DRIVER_NAME=nvidia
export XDG_SESSION_TYPE=wayland
export GBM_BACKEND=nvidia-drm
export __GLX_VENDOR_LIBRARY_NAME=nvidia
export SDL_VIDEODRIVER=wayland

export WLR_NO_HARDWARE_CURSORS=1
export QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_QPA_PLATFORMTHEME=qt5ct
export QT_QPA_PLATFORM=wayland
export GDK_DPI_SCALE=0.5
export MOZ_ENABLE_WAYLAND=1
export GTK_THEME=oomox-sandor-arc-dark
export GTK_ICON_THEME=oomox-sandor-arc-dark
export XDG_CURRENT_DESKTOP=hyprland
export _JAVA_AWT_WM_NONREPARENTING=1

export GDK_SCALE=2
export XCURSOR_SIZE=24

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 2>&1 >/dev/null &

dbus-launch Hyprland
