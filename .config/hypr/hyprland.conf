# See https://wiki.hyprland.org/Configuring/Monitors/
#monitor=DP-1,3840x2160,0x0,2
#monitor=DP-2,3840x2160,1920x0,2

monitor=,preferred,auto,2


workspace=DP-1,1
workspace=DP-2,11

workspace=1,monitor:DP-1
workspace=2,monitor:DP-1
workspace=3,monitor:DP-1
workspace=4,monitor:DP-1
workspace=5,monitor:DP-1
workspace=6,monitor:DP-1
workspace=7,monitor:DP-1
workspace=8,monitor:DP-1
workspace=9,monitor:DP-1
workspace=10,monitor:DP-1

workspace=11,monitor:DP-2
workspace=12,monitor:DP-2
workspace=13,monitor:DP-2
workspace=14,monitor:DP-2
workspace=15,monitor:DP-2
workspace=16,monitor:DP-2
workspace=17,monitor:DP-2
workspace=18,monitor:DP-2
workspace=19,monitor:DP-2
workspace=20,monitor:DP-2


$menu = tofi-drun --drun-launch=true

# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox
exec-once = /home/sandor/Projects/dotfiles/bin/autostart.sh
exec-once=xprop -root -f _XWAYLAND_GLOBAL_OUTPUT_SCALE 32c -set _XWAYLAND_GLOBAL_OUTPUT_SCALE 2
# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# toolkit-specific scale
env = GDK_SCALE,2
env = XCURSOR_SIZE,24

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us
    kb_options = 
    kb_variant =
    kb_model =
    kb_rules =

    follow_mouse = 1
    float_switch_override_focus = 0
    mouse_refocus=1

    touchpad {
        natural_scroll = no
    }

    sensitivity = -0.1 # -1.0 - 1.0, 0 means no modification.
}

xwayland {
  force_zero_scaling = true
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 4
    gaps_out = 0
    border_size = 2
    col.active_border = rgba(33ccffee) rgba(00ff55ee) 45deg
    col.inactive_border = rgba(595959aa)

    layout = master
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 5
    
    drop_shadow = no
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

misc {
    # groupbar_titles_font_size = 16
    # groupbar_gradients = false
}

animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 3, default, popin 80%
    animation = windowsOut, 1, 3, default, popin 80%
    animation = border, 1, 5, default
    animation = fade, 1, 3, default
    animation = workspaces, 1, 3, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = off
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
device:usb-gaming-mouse {
    sensitivity = -0.2
}

# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
windowrulev2 = float, title:^(LibreWolf — Sharing Indicator)$
windowrulev2 = move 10% 0, title:^(LibreWolf — Sharing Indicator)$
windowrulev2 = workspace special silent, title:^(.*is sharing (your screen|a window)\.)$
windowrulev2 = nomaximizerequest,class:^(libreoffice.*)$

# default workspace
windowrulev2 = workspace 3 silent, title:^(.*KeePassXC)$
windowrulev2 = workspace 3 silent, title:^(Nextcloud)$
windowrulev2 = workspace 11 silent, title:^(.* - Slack)$
windowrulev2 = workspace 12 silent, title:^(.* - Mozilla Thunderbird)$
windowrulev2 = center, title:^(App)
windowrulev2 = float, title:^(App)
windowrulev2 = center, title:^(Fyrox Game)
windowrulev2 = float, title:^(Fyrox Game)

# Audacious
windowrulev2 = float, title:^(Audacious)
windowrulev2 = float, title:^(.* - Audacious)

# idle inhibit while watching videos
windowrulev2 = idleinhibit focus, class:^(mpv|.+exe)$
windowrulev2 = idleinhibit focus, class:^(firefox)$, title:^(.*YouTube.*)$
windowrulev2 = idleinhibit fullscreen, class:^(firefox)$

# fix xwayland apps
windowrulev2 = rounding 0, xwayland:1, floating:1
windowrulev2 = center, class:^(.*jetbrains.*)$, title:^(Confirm Exit|Open Project|win424|win201|splash)$
windowrulev2 = size 640 400, class:^(.*jetbrains.*)$, title:^(splash)$

# Jetbrains products
windowrulev2 = float,floating:0,class:^(jetbrains-.*),title:^(win.*)
windowrulev2 = float,class:^(jetbrains-.*),title:^(Welcome to.*)
windowrulev2 = center,class:^(jetbrains-.*),title:^(Replace All)$
windowrulev2 = forceinput,class:^(jetbrains-.*)
windowrulev2 = windowdance,class:^(jetbrains-.*) # allows IDE to move child windows
windowrule = center, class:jetbrains-idea

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, RETURN, exec, alacritty
bind = $mainMod SHIFT, C, killactive, 
bind = $mainMod, ESCAPE, exec, /home/sandor/Projects/dotfiles/dmenu/shutdown.sh 
bind = $mainMod, E, exec, dolphin
bind = $mainMod, V, togglefloating, 
bind = $mainMod, R, exec, /home/sandor/Projects/dotfiles/dmenu/run.sh 
bind = $mainMod, P, exec, $menu
bind = $mainMod, T, togglegroup
bind = $mainMod, TAB, changegroupactive
bind = $mainMod SHIFT, P, pseudo, # dwindle
bind = $mainMod, J, togglesplit, # dwindle
bind = $mainMod SHIFT, Q, exit,

bind = $mainMod, print, exec, grim -g "$(slurp)" - | tee /tmp/screenshot-$(date +%Y-%m-%d_%H-%M-%S).png | wl-copy

# keyboard layout switcher
bind = $mainMod, A, exec, hyprctl keyword device:gaming-kb--gaming-kb-:kb_layout us & hyprctl keyword device:power-button:kb_layout us
bind = $mainMod, S, exec, hyprctl keyword device:gaming-kb--gaming-kb-:kb_layout hu & hyprctl keyword device:power-button:kb_layout hu

# volume
bindl = , xf86audioraisevolume, exec, pamixer --allow-boost -i 5
bindl = , xf86audiolowervolume, exec, pamixer --allow-boost -d 5
bindl = , xf86audiomute, exec, pamixer -t

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, exec,~/.cargo/bin/hyprsome workspace 1
bind = $mainMod, 2, exec,~/.cargo/bin/hyprsome workspace 2
bind = $mainMod, 3, exec,~/.cargo/bin/hyprsome workspace 3
bind = $mainMod, 4, exec,~/.cargo/bin/hyprsome workspace 4
bind = $mainMod, 5, exec,~/.cargo/bin/hyprsome workspace 5
bind = $mainMod, 6, exec,~/.cargo/bin/hyprsome workspace 6
bind = $mainMod, 7, exec,~/.cargo/bin/hyprsome workspace 7
bind = $mainMod, 8, exec,~/.cargo/bin/hyprsome workspace 8
bind = $mainMod, 9, exec,~/.cargo/bin/hyprsome workspace 9
bind = $mainMod, 0, exec,~/.cargo/bin/hyprsome workspace 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, exec, ~/.cargo/bin/hyprsome move 1
bind = $mainMod SHIFT, 2, exec, ~/.cargo/bin/hyprsome move 2
bind = $mainMod SHIFT, 3, exec, ~/.cargo/bin/hyprsome move 3
bind = $mainMod SHIFT, 4, exec, ~/.cargo/bin/hyprsome move 4
bind = $mainMod SHIFT, 5, exec, ~/.cargo/bin/hyprsome move 5
bind = $mainMod SHIFT, 6, exec, ~/.cargo/bin/hyprsome move 6
bind = $mainMod SHIFT, 7, exec, ~/.cargo/bin/hyprsome move 7
bind = $mainMod SHIFT, 8, exec, ~/.cargo/bin/hyprsome move 8
bind = $mainMod SHIFT, 9, exec, ~/.cargo/bin/hyprsome move 9
bind = $mainMod SHIFT, 0, exec, ~/.cargo/bin/hyprsome move 10

bind=$mainMod,period,exec,~/.cargo/bin/hyprsome focus r
bind=$mainMod,comma,exec,~/.cargo/bin/hyprsome focus l
bind=$mainMod SHIFT,period,exec,~/.cargo/bin/hyprsome tagmon r
bind=$mainMod SHIFT,comma,exec,~/.cargo/bin/hyprsome tagmon l

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow
