require("config.lazy")

require("config.options")
require("config.keymaps")

require('lualine').setup {
    options = {
        icons_enabled = true,
        theme = 'dracula',
        section_separators = '', component_separators = ''
    }
}
