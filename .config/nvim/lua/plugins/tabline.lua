return {
    {
		"crispgm/nvim-tabline",
		config = function()
			require("tabline").setup({
				show_index = true,
				show_modify = true,
				show_icon = true,
				modify_indicator = "*",
				no_name = "<untitled>",
				brackets = { "[", "]" },
			})
		end,
		dependencies = { "nvim-tree/nvim-web-devicons" },
	},
}
