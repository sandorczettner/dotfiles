local opts = { noremap = true, silent = true }

local keymap = vim.api.nvim_set_keymap

local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = 'Telescope find files' })
vim.keymap.set('n', '<leader>fg', builtin.live_grep, { desc = 'Telescope live grep' })
vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = 'Telescope buffers' })
vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = 'Telescope help tags' })
vim.keymap.set('n', '<leader>fp', require'telescope'.extensions.projects.projects, { desc = 'Telescope find projects' })

-- Naviagate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)
keymap("n", "<S-Right>", ":bnext<CR>", opts)
keymap("n", "<S-Left>", ":bprevious<CR>", opts)

-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

vim.keymap.set('n', '<leader>b', '<Cmd>Neotree toggle<CR>')

-- Define :SudoW command to save the file using pkexec and force reload
vim.api.nvim_create_user_command('SudoW', function()
    vim.cmd('silent! w !pkexec tee % >/dev/null') -- Save file using pkexec
    vim.cmd('edit!')  -- Force reload the file
end, {})
-- Map 'w!!' to save the file using pkexec
vim.api.nvim_set_keymap('c', 'w!!<CR>', "<esc>:SudoW<CR>", { silent = true, noremap = true })

