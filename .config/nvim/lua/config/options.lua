vim.g.autoformat = true
vim.cmd[[colorscheme dracula]]

local opt = vim.opt
opt.hlsearch           = false
opt.ignorecase         = true                          -- Ignore case when using lowercase in search
opt.smartcase          = true                          -- But don't ignore it when using upper case
opt.smarttab           = true
opt.smartindent        = true
opt.expandtab          = true                          -- Convert tabs to spaces.
opt.tabstop            = 4
opt.softtabstop        = 4
opt.shiftwidth         = 4
opt.splitbelow         = true
opt.splitright         = true
opt.scrolloff          = 12                            -- Minimum offset in lines to screen borders
opt.sidescrolloff      = 8
opt.relativenumber     = true
opt.mouse              = 'a'
opt.clipboard          = 'unnamedplus'

-- Neovide
vim.o.guifont = "Hack Nerd Font:h10"
