-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This will hold the configuration.
local config = wezterm.config_builder()

local mappings = require 'mappings'

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
config.color_scheme = 'Darkside'
config.hide_tab_bar_if_only_one_tab = true
-- config.font = wezterm.font 'Hack Nerd Font Mono'
config.font_size = 10.5
config.disable_default_key_bindings = true
config.keys = mappings.keys
config.key_tables = mappings.key_tables
config.enable_wayland = true
config.window_decorations = "NONE"
config.front_end = "WebGpu"
config.window_background_opacity = 0.93
config.adjust_window_size_when_changing_font_size = false

-- and finally, return the configuration to wezterm
return config
