#!/bin/bash
#
# a simple dmenu session script 
#
###

# An array of options to choose.
declare -a options=(
"slack"
"idea"
"abevjava"
"dbeaver"
"insomnia"
"librewolf -p personal"
"librewolf -p sitesquad"
"librewolf -p ascend"
"steam"
"doom"
"quake 2"
"quake 2: xatrix"
"quake 2: rogue"
"quakespasm"
"quake: scourge of armagon"
"quake: dissolution of eternity"
"quake: dimension of the machine"
"quake: dimension of the past"
"quake: arcane dimensions"
)

# Piping the above array into dmenu.
# We use "printf '%s\n'" to format the array one item to a line.
mplayer ~/Music/Sounds/keytry.wav &
#choice=$(printf '%s\n' "${options[@]}" | dmenu -fn 'Hack Nerd Font:pixelsize=22:antialias=true:autohint=true' -nb '#303842' -nf '#abb2bf' -sb '#000000' -sf '#00b300' -i -p 'Menu:' "${@}")
#choice=$(printf '%s\n' "${options[@]}" | wofi --show dmenu -p 'Menu:' "${@}")
choice=$(printf '%s\n' "${options[@]}" | tofi)

case "$choice" in
  "slack")
    slack --ozone-platform-hint=auto --enable-features=WebRTCPipeWireCapturer &
    ;;
  "idea")
    _JAVA_AWT_WM_NONREPARENTING=1 idea &
    ;;
  abevjava)
    export _JAVA_AWT_WM_NONREPARENTING=1
    abevjava &
    ;;
  dbeaver)
    ~/dbeaver.sh &
    ;;
  "steam")
    gamescope -W 1920 -H 1080 -r 140 -e -- steam -gamepadui
    ;;
  "doom")
    gzdoom -file ~/.config/gzdoom/cheello_voxels.zip &
    ;;
  "quake 2")
    yamagi-quake2 &
    ;;
  "quake 2: xatrix")
    yamagi-quake2 +set game xatrix &
    ;;
  "quake 2: rogue")
    yamagi-quake2 +set game rogue &
    ;;
  "quake: scourge of armagon")
    ironwail -basedir ~/.quakespasm -game hipnotic
    ;;
  "quake: dissolution of eternity")
    ironwail -basedir ~/.quakespasm -game rogue
    ;;
  "quake: arcane dimensions")
    ironwail -basedir ~/.quakespasm -game ad -heapsize 650000 -zone 4096 -sndspeed 44100
    ;;
  "quake: dimension of the machine")
    ironwail -basedir ~/.quakespasm -game mg1
    ;;
  "quake: dimension of the past")
    ironwail -basedir ~/.quakespasm -game dopa
    ;;
  "quakespasm")
    ironwail -basedir ~/.quakespasm
    ;;
  insomnia) insomnia & ;;
  *) # Default, execute the choice as it is
    $choice &
    ;;
esac
