#!/bin/bash
#
# a simple dmenu session script 
#
###

# An array of options to choose.
declare -a options=(
"dpms"
"suspend"
"logout"
"reboot"
"shutdown"
)

# Piping the above array into dmenu.
# We use "printf '%s\n'" to format the array one item to a line.
mplayer ~/Music/Sounds/keytry.wav &
#choice=$(printf '%s\n' "${options[@]}" | dmenu -fn 'Hack Nerd Font:pixelsize=22:antialias=true:autohint=true' -nb '#303842' -nf '#abb2bf' -sb '#000000' -sf '#00b300' -i -p 'Shutdown menu:' "${@}")
choice=$(printf '%s\n' "${options[@]}" | tofi)

case "$choice" in
  logout) hyprctl dispatch exit 1;;
  shutdown) loginctl poweroff & ;;
  reboot) loginctl reboot & ;;
  suspend)
    mplayer ~/Music/Sounds/butn2.wav &
    loginctl suspend &
    ;;
  dpms)
    swaylock &
    sleep 0.5
    ~/.local/bin/dpms-off
    ;;
esac
