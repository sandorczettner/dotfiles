# Sandor's dotfiles

OS: Artix Linux x86_64

WM: dwm

Shell: zsh 5.8

![Screenshot](images/screenshot.png "DWM Screenshot")

## Colour theme

```css
.background {
    background-color: #2B3A55
}
.alternate {
    background-color: #483434;
}
.text {
    color: #E2DEAA;
}
.bright {
    color: #FAECD6;
}
.primary {
    color: #4E6C50;
}
.warning {
    color: #923010;
}

.test {
    color: #ff79c6;
}
```

## NeoVim

Before you start, run:
```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

On first run, install the packages using the `:PackerSync` command and restart nvim.

NeoVim is set up with LSP and various language servers. To install the prerequisites, execute:

```
npm i -g vscode-langservers-extracted
npm i -g intelephense
npm i -g pyright
gem install solargraph
pacman -S lua-language-server
pacman -S deno
```
This requires node v16

### A few shortcuts in nvim:

:w!!        - sudo save
C-p         - fuzzy find
C-t         - grep finder
Tab         - switch tab
C-hjkl      - navigate around windows

space, b    - Toggle NeoTree
space, r    - Show current file in NvimTree
space, f,p  - Switch projects
space, 1    - switch to the first tab. 2 will switch to the second, etc

### Inside NvimTree:
x           - add/remove file/directory to cut clipboard
c           - add/remove file/directory to copy clipboard
y           - copy name to system clipboard
Y           - copy relative path to system clipboard
gy          - copy absolute path to system clipboard
p           - paste from clipboard.
C-v         - open the file in a vertical split
C-x         - open the file in a horizontal split
C-t         - open the file in a new tab
Tab         - open the file as a preview (keep NvimTree active)

### Tips and tricks

Format a JSON:
```
:%!jq '.'
```
