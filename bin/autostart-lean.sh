#!/bin/sh
pipewire &
pipewire-pulse &
wireplumber &

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 2>&1 >/dev/null &
waybar -c ~/.config/waybar/config-niri &
/home/sandor/Projects/dotfiles/bin/suspend-niri.sh &
