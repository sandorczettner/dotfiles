#!/bin/sh
waybar &
fnott &
wbg /usr/share/wallpapers/forest.jpg &
nm-applet --indicator &
/home/sandor/Projects/dotfiles/bin/suspend.sh &

pipewire &
pipewire-pulse &
wireplumber &

nextcloud &
keepassxc &
thunderbird &
/usr/bin/slack --enable-features=WebRTCPipeWireCapturer --enable-features=UseOzonePlatform --ozone-platform=wayland &

killall -e xdg-desktop-portal-hyprland
killall -e xdg-desktop-portal-wlr
killall xdg-desktop-portal
/usr/lib/xdg-desktop-portal-hyprland &
/usr/lib/xdg-desktop-portal &
